# apt-config AutoUpdate

A few configuration snippets for the APT package manager to
have it automatically update its package cache.
This is useful for systems which do not use PackageKit for triggering
cache refreshes from the desktop environment.
